@echo off
set thisdir=%~dp0
rem call "%thisdir%set_environment.bat"
set MATLAB_ROOT="C:\Program Files\MATLAB\R2015b\bin\matlab.exe"
set PSYCHTOOLBOX_ROOT=C:\Toolbox\Psychtoolbox

rem get current date and time, though not used .....
FOR /F "TOKENS=1* DELIMS= " %%A IN ('DATE/T') DO SET CDATE=%%B 
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set date=%%c%%a%%b) 
FOR /F "TOKENS=1* DELIMS= " %%A IN ('TIME/T') DO SET CTIME=%%B 
For /f "tokens=1-4 delims=: " %%a in ('time /t') do (set time=%%a%%b%%c) 
set timestamp=%date%_%time%

echo starting Matlab to perform Psychtoolbox video test .... 			
start "Psychtoolbox check" /WAIT %MATLAB_ROOT% -nodesktop -r "chdir('%USERPROFILE%'); disp(['fld=' '%PSYCHTOOLBOX_ROOT%']); addpath(genpath('%PSYCHTOOLBOX_ROOT%')); addpath('%PSYCHTOOLBOX_ROOT%\PsychBasic\MatlabWindowsFilesR2007a\'); try, VBLSyncTest(); catch, disp('Psychtoolbox VBLSyncTest failed!'); end, disp('Press a key to abandon Psychtoolbox test'); pause(); exit;" 

rem a trick to wait until Matlab has finished executing PTB performance (VBLSyncTest) script
:loop
timeout 1 | rem
tasklist /fi "imagename eq MATLAB.exe" |find ":" > nul
if errorlevel 1 goto loop

echo Psychtoolbox check has been completed.

rem todo: return relevant video parameters, via performance log folder????
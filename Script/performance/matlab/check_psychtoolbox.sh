#!/bin/bash

cd `dirname ${BASH_SOURCE[0]}`
source ./set_environment.sh

if [ `uname -s` == 'Linux' ]; then
	OS='glnxa64'
else # Mac
	OS='maci64'
fi
			
echo starting Matlab to perform Psychtoolbox video test .... 			
$MATLAB_ROOT -nodesktop -r "chdir('$HOME'); disp(['fld=' '$PSYCHTOOLBOX_ROOT']); addpath(genpath('$PSYCHTOOLBOX_ROOT')); try, VBLSyncTest(); catch, disp('Psychtoolbox VBLSyncTest failed!'); end, disp('Press a key to abandon Psychtoolbox test'); pause(); exit;"

echo Psychtoolbox check has been completed.

# todo: return relevant video parameters, via performance log folder????

	


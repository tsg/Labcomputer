#!/bin/bash
#gtf 1024 768 144
xrandr --newmode "1024x768_144.00"  169.30  1024 1104 1216 1408  768 769 772 835  -HSync +Vsync
xrandr --addmode DVI-I-0 1024x768_144.00
xrandr --output DVI-I-0 --mode 1024x768_144.00
